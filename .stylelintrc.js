module.exports = {
  extends: [
    '@up-line/stylelint-config',
    'stylelint-config-recommended-vue/scss',
  ],
};
