import { defineStore } from 'pinia';

export const useStore = defineStore('main', {
  state: () => ({
    url: 'https://laravel.kiosk.ecoplant-tech.ru/',
    managers: [],
    selectManager: null,
    plants: {},
    fertilizers: {},
    cart: {
      plants: {},
      fertilizers: {},
      fertilizersIds: [],
    },
    currentOrder: {
      products: [],
      manager: '',
    },
  }),
  actions: {
    changeFertilizers(id, value) {
      if (value === 0) {
        delete this.cart.fertilizers[id];
        return;
      }
      this.cart.fertilizers[id] = value;
    },
    changePlants(idPlant, idSizePlant, value) {
      if (this.cart.plants[idPlant] && value === 0) {
        delete this.cart.plants[idPlant][idSizePlant];
        if (Object.keys(this.cart.plants[idPlant]).length === 0) {
          delete this.cart.plants[idPlant];
        }
        return;
      }
      if (this.cart.plants[idPlant]) {
        this.cart.plants[idPlant][idSizePlant] = value;
        return;
      }
      if (value !== 0) {
        this.cart.plants[idPlant] = { [idSizePlant]: value };
      }
    },
    addFertilizersList(id) {
      if (this.cart.fertilizersIds.includes(id)) {
        return;
      }
      this.cart.fertilizersIds.push(id);
    },
    removeFertilizersList(id) {
      this.cart.fertilizersIds = this.cart.fertilizersIds.filter(
        (e) => e !== id
      );
    },
    setCurrentOrder(type) {
      if (type === 'fertilizers') {
        this.currentOrder.products = this.getCartFertilizers;
      } else if (type === 'plants') {
        this.currentOrder.products = this.getCartPlants;
      }
    },
    setCurrentManager(id) {
      for (const value of Object.values(this.managers)) {
        if (id === value.id) {
          this.currentOrder.manager = value;
        }
      }
    },
  },
  getters: {
    getStoreManagers: (state) => state.managers,
    getCurrentOrder: (state) => state.currentOrder.products,
    getCartFertilizers: (state) => {
      const cartFertilizersArray = [];
      for (const id of state.cart.fertilizersIds) {
        cartFertilizersArray.push({
          product: state.fertilizers[id],
          quantity: state.cart.fertilizers[id],
        });
      }
      return cartFertilizersArray;
    },
    getCartPlants: (state) => {
      const cartPlantsArray = [];
      for (const [plantId, plantSizes] of Object.entries(state.cart.plants)) {
        for (const [plantSizeId, quantity] of Object.entries(plantSizes)) {
          let plant = {};
          state.plants[plantId].plants.forEach((item) => {
            if (item.id === Number(plantSizeId)) {
              plant = item;
            }
          });
          cartPlantsArray.push({
            product: plant,
            quantity,
            shortName: state.plants[plantId].short_name,
          });
        }
      }
      return cartPlantsArray;
    },
    totalPrice: (state) => {
      let total = 0;
      for (const [key, value] of Object.entries(state.cart.fertilizers)) {
        total += state.fertilizers[key].price * value;
      }
      return total;
    },
    sortFertilizers: (state) => {
      if (Object.keys(state.fertilizers).length === 0) {
        return;
      }
      return Object.values(state.fertilizers).sort((a, b) => {
        const nameA = a.name.toLowerCase();
        const nameB = b.name.toLowerCase();
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
      });
    },
    fertilizersByLetters() {
      const groups = [];
      let group;
      if (Object.keys(this.fertilizers).length === 0) {
        return;
      }
      const sortFertilizersObj = this.sortFertilizers;
      group = {
        letter: sortFertilizersObj[0].name[0].toLowerCase(),
        fertilizerId: sortFertilizersObj[0].id,
      };
      groups.push(group);
      for (const value of sortFertilizersObj) {
        if (value.name[0].toLowerCase() !== group.letter) {
          group = {
            letter: value.name[0].toLowerCase(),
            fertilizerId: value.id,
          };
          groups.push(group);
        }
      }
      return groups;
    },
  },
});
